#!/bin/bash

source conf
source rmOptTools.sh

# Override the configuration if found

if [ -r ~/.netkitMPTCP ]; then
  source ~/.netkitMPTCP
fi

usage()
{
cat << EOF
usage: $0 options

OPTIONS:
	-l		Set the left /24 subnet to l
	-r		Set the right /24 subnet to r
	-n		Set the number of interfaces on the client side
	-m		Set the number of interfaces on the server side
	-k		Set a kernel for the client and the server, this time only (more perm change can be done in your ~/.netkitMPTCP)
	-R		Change the removed opt in the noNat configuration
			Model : (L|R)(S|D|B)((T|M)([0-9]+/)*([0-9]+){1})+
	-h		Help !

default options : -l "42.42.42" -r "123.123.123" -n 1 -m 1
EOF
}

#
# Note : 
#  -> Right refer to the server side
#  -> Left refer to the client side
#

# The path to the click binary
CLICK=$GITPATH/netkitClick/click

# The path (relative to home/$USER) to a directory that contains click configuration files
CLICKCONF=$GITPATH/example

FTPCONF=$GITPATH/misc/proftpd.conf

# Ethernet used on click router, 
LEFTETHER=BA:89:41:C3:61:D7
RIGHTETHER=0E:AA:C0:D6:60:AC

# Client/Router/Server name
CLIENT=r1
ROUTER=r2
SERVER=r3

LEFTSUBNET="42.42.42"
RIGHTSUBNET="123.123.123"
CLIENTIF=1
SERVERIF=1
CLEAN=

loadParam "L$RMLEFT"
loadParam "R$RMRIGHT"

while getopts “chl:r:m:n:k:R:” OPTION
do
	case $OPTION in
		h)
			usage
			exit 1
			;;
		l)
			LEFTSUBNET=$OPTARG
			;;
		r)
			RIGHTSUBNET=$OPTARG
			;;
		n)
			CLIENTIF=$OPTARG
			;;
		m)
			SERVERIF=$OPTARG
			;;
		c)
			CLEAN=1
			;;
		k)
			MPTCP_KERNEL=$OPTARG
			;;
		R)
			loadParam $OPTARG
			;;
		?)
			usage
			exit
			;;
	esac
done

if [[ -z $LEFTSUBNET ]] || [[ -z $RIGHTSUBNET ]] || [[ -z $CLIENTIF ]] || [[ -z $SERVERIF ]]
then
	echo -e "\nAll options are mendatory, except the help... \n"
	echo "l : $LEFTSUBNET"
	echo "r : $RIGHTSUBNET"
	echo "n : $CLIENTIF"
	echo -e "m : $SERVERIF\n"
	usage
	exit 1
fi

if [[ ! -z $CLEAN ]]
then
	echo "will clean before the generation of the configuration files ! "
	lclean
fi

let SERVERIF+=1
let CLIENTIF+=1

rm -rf lab.conf
rm -rf $CLIENT.startup
rm -rf $SERVER.startup
rm -rf $ROUTER.startup
rm -rf $CLIENT
rm -rf $SERVER
rm -rf $ROUTER
mkdir $CLIENT
mkdir $SERVER
mkdir $ROUTER

#
# Build the client
#

for i in $(seq 2 1 $CLIENTIF)
do
	let ii=i-1
	echo "/sbin/ifconfig eth$ii $LEFTSUBNET.$i netmask 255.255.255.0 broadcast $LEFTSUBNET.255 up" >>  $CLIENT.startup
done

echo "sysctl -w net.mptcp.mptcp_debug=1" >> $CLIENT.startup

echo "route add -net $RIGHTSUBNET.0 netmask 255.255.255.0 gw $LEFTSUBNET.1" >>  $CLIENT.startup


#
# Build the server
#

for i in $(seq 2 1 $SERVERIF)
do
	let ii=i-1
	echo "/sbin/ifconfig eth$ii $RIGHTSUBNET.$i netmask 255.255.255.0 broadcast $RIGHTSUBNET.255 up" >>  $SERVER.startup
done

echo "sysctl -w net.mptcp.mptcp_debug=1" >> $SERVER.startup

echo "route add -net $LEFTSUBNET.0 netmask 255.255.255.0 gw $RIGHTSUBNET.1" >>  $SERVER.startup
echo "cp /hosthome/$FTPCONF /etc/proftpd/" >>  $SERVER.startup
echo "/etc/init.d/proftpd start" >>  $SERVER.startup

#
# Build the router
#

echo "/sbin/ifconfig eth3 $LEFTSUBNET.1 netmask 255.255.255.0 broadcast $LEFTSUBNET.255 hw ether $LEFTETHER promisc up"  >>  $ROUTER.startup
echo "/sbin/ifconfig eth4 $RIGHTSUBNET.1 netmask 255.255.255.0 broadcast $RIGHTSUBNET.255 hw ether $RIGHTETHER promisc up"  >>  $ROUTER.startup
echo "sysctl net.ipv4.ip_forward=0"  >>  $ROUTER.startup
echo "echo export PATH=/root:\\\$PATH >> /root/.bashrc"  >>  $ROUTER.startup
echo "cp /hosthome/$CLICK /root/click" >>  $ROUTER.startup
echo "cp /hosthome/$CLICKCONF/* /root/" >>  $ROUTER.startup

cat >>  $ROUTER.startup << EOF
for f in /root/*.click;
do
	sed -e 's/AddressInfo(Left l.l.l/AddressInfo(Left $LEFTSUBNET/' -e 's/AddressInfo(Right r.r.r/AddressInfo(Right $RIGHTSUBNET/'< \$f > \$f.tmp;
	mv \$f.tmp \$f;
done
sed -e 's/--LOPT--/"$LOPT"/' 		\\
	 -e 's/--ROPT--/"$ROPT"/' 		\\
	 -e 's/--LMOPT--/"$LMOPT"/'  	\\
	 -e 's/--RMOPT--/"$RMOPT"/' 	\\
	 -e 's/--LSYN--/$LSYN/' 		\\
	 -e 's/--LDATA--/$LDATA/' 	\\
	 -e 's/--RSYN--/$RSYN/' 		\\
	 -e 's/--RDATA--/$RDATA/' < /root/noNatRmOpt.click > /root/noNatRmOpt.clicktmp
mv /root/noNatRmOpt.clicktmp /root/noNatRmOpt.click

EOF



#
# lab.conf
#

cat >> lab.conf << EOF
LAB_DESCRIPTION="MiddleBoxes 1"
LAB_VERSION="0.1"
LAB_AUTHOR="Bhesmans"  
LAB_EMAIL="None"
LAB_WEB="None"    

machines="r1 r2 r3"

r1[0]=192.168.1.0.30
r2[0]=192.168.1.0.30
r2[1]=192.168.1.4.30
r3[0]=192.168.1.4.30


r2[3]=left
r2[4]=right

r2[mem]=128

EOF

for i in $(seq 2 1 $CLIENTIF)
do
	let ii=i-1
	echo "$CLIENT[$ii]=left"  >>  lab.conf
done

for i in $(seq 2 1 $SERVERIF)
do
	let ii=i-1
	echo "$SERVER[$ii]=right"  >>  lab.conf
done

echo "$CLIENT[kernel]=$MPTCP_KERNEL"  >>  lab.conf
echo "$SERVER[kernel]=$MPTCP_KERNEL"  >>  lab.conf










