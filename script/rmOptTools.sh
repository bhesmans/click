#!/bin/bash

#
# get the option String in $1 starting from $2 up to the end OR T or M
#
returnTOpt=""
returnMOpt=""

function getOptRm {
	i=0
	max=${#1}
	let max=max-1
	s=$2
	let s=s+1
	for n in `seq $s 1 $max` ; do
		if [  ${1:$n:1} = "T" -o ${1:$n:1} = "M" ]; then
			returnOpt=${1:$s:$i}
			return $n
		fi
		let i=i+1
	done
	returnOpt=${1:$s:$i}
	return $max
}

function loadOneOpt {
	getOptRm $1 $2
	r=$?
	if [ ${1:$2:1} = "T" ]; then
		tOpt=$returnOpt
	elif [ ${1:$2:1} = "M" ]; then
		mOpt=$returnOpt
	fi
	return $r
}

function loadOpt {
	tOpt=
	mOpt=

	loadOneOpt $1 2
	r=$?
	loadOneOpt $1 $r

	echo "TCP    $tOpt"
	echo "MPTCP  $mOpt"

}

function loadParam {
	if [ ${#1} -le 2 ]; then
		#Too short, won't affect anything
		return 1
	fi	
	param=$1
	loadOpt $param
	if [ ${1:0:1} = "L" ]; then
		echo "from the left side to the right side"
		LOPT=$tOpt
		LMOPT=$mOpt
		if [ ${1:1:1} = S ]; then
			LSYN=true
			LDATA=false
		elif [ ${1:1:1} = D ]; then
			LSYN=false
			LDATA=true
		elif [ ${1:1:1} = B ]; then
			LSYN=true
			LDATA=true
		fi
	elif [ ${1:0:1} = "R" ]; then
		echo "from the right side to the left side"
		ROPT=$tOpt
		RMOPT=$mOpt
		if [ ${1:1:1} = S ]; then
			RSYN=true
			RDATA=false
		fi
		if [ ${1:1:1} = D ]; then
			RSYN=false
			RDATA=true
		fi
		if [ ${1:1:1} = B ]; then
			RSYN=true
			RDATA=true
		fi
	fi
}







