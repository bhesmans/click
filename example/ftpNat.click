AddressInfo(Left l.l.l.1 BA:89:41:C3:61:D7);
AddressInfo(Right r.r.r.1 0E:AA:C0:D6:60:AC);

eth3ArpQ::ARPQuerier(Left)
eth4ArpQ::ARPQuerier(Right)

eth3::FromDevice(eth3, SNIFFER false, PROMISC true) 
eth4::FromDevice(eth4, SNIFFER false, PROMISC true) 

ceth3::Classifier(12/0806 20/0001, 	// Arp Querry
 12/0806 20/0002,			// Arp Response
 12/0800 23/01,				// ICMP
 12/0800 23/06,				// TCP
 12/0800,
 -);
ceth4::Classifier(12/0806 20/0001, 	// Arp Querry
 12/0806 20/0002,			// Arp Response
 12/0800 23/01,				// ICMP
 12/0800 23/06,				// TCP
 12/0800,
 -);

ftpCtlClass::Classifier(22/0015,	// Control connection
-
)

/*
 * Ping rewrite. Rem : only one direction ping is available with this configuration,  this is the standard nat behavior
 * (if you add the second pattern Left - 1 0, the other side will be able to ping too.)
 */
pingRW::ICMPPingRewriter(pattern Right - 0 1) 

/*
 * TCPRewriter
 * 
 */
tcpRW::TCPRewriter(pattern Right 15000-16000 - - 0 1, pass 2)

/*
 * FTP Port Rewriter
 */

ftpRW::FTPPortMapper(tcpRW, tcpRW, 0)

/*
 * First classification and early concerns
 */

eth3 -> ceth3
eth4 -> ceth4

ceth3[0] -> Discard 								// already responded ...
ceth3[1] -> [1]eth3ArpQ 							// feed our Querrier
ceth3[4] -> Print(ipOnEth3Unknown) -> Discard		// Should not happen
ceth3[5] -> Print(notIpOnEth3Unknown) -> Discard 	// Should not happen

ceth4[0] -> Discard 								// already responded ...
ceth4[1] -> [1]eth4ArpQ 							// feed our Querrier
ceth4[4] -> Print(ipOnEth4Unknown) -> Discard		// Should not happen
ceth4[5] -> Print(notIpOnEth4Unknown) -> Discard 	// Should not happen

/*
 * The actual Job
 */

/*
 * ICMP special rewritting
 */ 
ceth3[2] -> Strip(14) -> CheckIPHeader() -> pingRW -> eth4ArpQ -> Queue -> ToDevice(eth4)
ceth4[2] -> Strip(14) -> CheckIPHeader() -> pingRW[1] -> eth3ArpQ -> Queue -> ToDevice(eth3)

/*
 * TCP rewritting
 */ 

ceth3[3] -> Strip(14) -> CheckIPHeader()
-> ftpCtlClass   -> ftpRW -> tcpRW
ftpCtlClass[1] -> tcpRW -> eth4ArpQ



ceth4[3] -> Strip(14) -> CheckIPHeader() ->  [1]tcpRW[1] -> eth3ArpQ

tcpRW[2] -> Print(noMapping) -> Discard
