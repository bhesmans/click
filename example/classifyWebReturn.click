eth1ArpQ::ARPQuerier(192.168.1.5, 12:3E:E2:7D:E3:87)
eth0ArpQ::ARPQuerier(192.168.1.2, 3A:40:EE:31:9E:CD)
tapArpQ::ARPQuerier(172.16.0.3,4A:6C:31:ED:D1:DB)


ceth0::Classifier(12/0806 20/0001,
 12/0806 20/0002,
 12/0800 30/c0a80106,
 12/0800,
 -);

ceth1::Classifier(12/0806 20/0001,
 12/0806 20/0002,
 12/0800 30/c0a80101,
 -);

ctap::Classifier(12/0806 20/0001,
 12/0806 20/0002,
 12/0800 30/c0a80101,
 -);

eth1::FromDevice(eth1, SNIFFER false, PROMISC true) -> ceth1

ceth1[0]-> Print(p5ArpRequest) -> Discard
ceth1[1]-> Print(p5ArpResponder) -> [1]eth1ArpQ
ceth1[2] //-> Print(p5192.168.1.1)
-> Strip(14) -> CheckIPHeader()  
-> c1tp::IPClassifier(ip proto tcp,-)
-> RemoveTCPOpt(KILLNOTINSPECTED false, TCPOPTIONS 3, VERBOSE false, RMSYN true)
-> SetTCPChecksum
-> eth0ArpQ
c1tp[1]->eth0ArpQ
-> Queue -> Print(qout1) ->  ToDevice(eth0)
ceth1[3]-> Print(p5Trash) -> Discard


eth0::FromDevice(eth0, SNIFFER false, PROMISC true) -> ceth0

ceth0[0]-> Print(p6ArpRequest) -> Discard
ceth0[1]-> Print(p6ArpResponder) -> [1]eth0ArpQ
ceth0[2] // -> // Print(p6192.168.1.6)
-> Strip(14) 
-> CheckIPHeader()
-> eth1ArpQ
-> Queue -> Print(qout6) ->  ToDevice(eth1)
ceth0[4]-> Print(p6Trash) -> Discard

tap::FromDevice(eth2, SNIFFER false, PROMISC true) -> ctap

ctap[0] -> Discard
ctap[1] -> [1]tapArpQ
ctap[2]
-> Strip(14) 
-> CheckIPHeader()
-> eth0ArpQ
// -> Queue -> Print(qout6) ->  ToDevice(eth0)

ceth0[3]
//-> Strip(14) 
-> CheckIPHeader(OFFSET 14)
//-> Print(beforeRewrite) -> rw::IPRewriter(pattern 172.16.0.3 32000-33000 - - 1 1,MAPPING_CAPACITY 10)[1] -> Print(IPREWRITE)
-> rw::IPAddrPairRewriter(pattern 172.16.0.3 - - 0 1) -> Print(outoffiprw)
-> Strip(14)
-> CheckIPHeader()
-> c0tp::IPClassifier(ip proto tcp,-)
-> RemoveTCPOpt(KILLNOTINSPECTED false, TCPOPTIONS 42, VERBOSE true, RMSYN true)
-> SetTCPChecksum
-> sa::SetIPAddress(172.16.0.1)
-> tapArpQ -> Queue -> Print(tapout) ->  ToDevice(eth2)
c0tp[1]->sa


rw[1] -> Print(iprwretruned) -> Strip(14)
-> CheckIPHeader()
-> cTAPtp::IPClassifier(ip proto tcp,-)
-> RemoveTCPOpt(KILLNOTINSPECTED false, TCPOPTIONS 8, VERBOSE false, RMSYN true, DELAY 10, TTL 60)
-> SetTCPChecksum
->  eth0ArpQ

cTAPtp[1]->eth0ArpQ

ctap[3] ->  CheckIPHeader(OFFSET 14) ->  Print(iptorewriteonreturn) -> rw

//rw[0] -> Print(rwTrash) -> Discard


